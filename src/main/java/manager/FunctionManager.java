package manager;

public class FunctionManager {

    public int increment(int x) {
        if (x != Integer.MAX_VALUE) {
            return ++x;
        }
        throw new RuntimeException("value is too high to store in int");
    }
}
