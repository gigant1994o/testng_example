import manager.FunctionManager;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class FunctionsTest {

    private FunctionManager manager;

    @BeforeTest
    public void init() {
        manager = new FunctionManager();
    }

    @Test
    public void testIncrementWithNormalValue() {
        assertEquals(manager.increment(1), 2);
    }

    @Test(expectedExceptions = RuntimeException.class)
    public void testIncrementWithNotNormalValue() {
        manager.increment(Integer.MAX_VALUE);
    }

}
